import 'package:flutter/material.dart';
import 'package:my_slivers/sliver_app_bar/sliver_app_bar.dart';
import 'package:my_slivers/sliver_list_and_sliver_grid/sliver_list_and_sliver_app_bar.dart';
import 'package:my_slivers/widgets/my_menu_button.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Sliver Flutter',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.orange,
      ),
      home: HomePage(),
    );
  }
}

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: Text('Slivers'),
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          MyMenuButton(
            title: 'SliverAppBar',
            actionTap: () {
              onButtonTap(
                SLiverAppBarPage(),
              );
            },
          ),
          MyMenuButton(
            title: 'SliverList and SliverGrid',
            actionTap: () {
              onButtonTap(
                SliversBasicPage(),
              );
            },
          ),
        ],
      ),
    );
  }

  void onButtonTap(Widget page) {
    Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) => page));
  }
}
