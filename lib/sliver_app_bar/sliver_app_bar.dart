import 'package:flutter/material.dart';
import 'package:my_slivers/res/consts.dart';
import 'package:my_slivers/sliver_app_bar/custom_page_header.dart';


class SLiverAppBarPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: CustomScrollView(
        slivers: <Widget>[
          SliverPersistentHeader(
            pinned: false,
            floating: true,
            delegate: CustomHeader(
              maxExtent: 350.0,
              minExtent: 100.0,
            ),
          ),
          SliverList(
            delegate: SliverChildListDelegate(
              <Widget>[
                Container(
                  padding: const EdgeInsets.symmetric(vertical: 20.0, horizontal: 20.0),
                  child: Text(
                    Consts.kText,
                    style: TextStyle(fontSize: 22.0),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
