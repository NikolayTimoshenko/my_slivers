import 'package:flutter/material.dart';

class MyMenuButton extends StatelessWidget {
  final String title;
  final VoidCallback actionTap;


  MyMenuButton({this.title, this.actionTap});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(15.0),
      child: MaterialButton(
        height: 50.0,
        color: Theme.of(context).primaryColor,
        textColor: Colors.white,
        child: Text(title),
        onPressed: actionTap,
      ),
    );
  }
}